require 'open-uri'
require 'mechanize'

module Voter
  class Clicker
      attr_accessor :page, :form, :button
    def initialize(url = 'http://bloknot-volgograd.ru/contests/primaries-readers-notepad-volgograd-633848/')
      @url = url
      @agent = Mechanize.new
    end

    def click
      getButtons
      hash = { '3100': 0, '3132': 2, '3112': 0,'3128': 4, '3126': 4, '3102': 4, '3114': 0, '3108': -1 }
      # 3100 - федюшкин, 3132 - волоцков, 3112 - паршин, (3 место) 3128 - гусева, (5 место) 3102 - трубина
      # 
      if hash[@button.value.to_sym]
          key = hash[@button.value.to_sym]
          random = rand(1..key) if key > 0
          random = rand(key..1) if key < 1
          while random > 0
              @agent.submit(@form, @button)
              random -= 1;
              sleep 5
              puts "ELITE MAN #{@button.value}"
          end
      else
          #@agent.submit(@form, @button)
          #puts "JUST MAN #{@button.value}"
      end
    end

    def getButtons
        #while 1 == rand(0..3) random = rand(0..19)
      @agent.cookie_jar.clear! # clear cookie
      page = lambda { @agent.get(@url) }
      @form = page.call.form_with(class: 'vote-form')
      @button = @form.buttons[rand(0..19)]
    end
  end
end

def set_interval(delay)
  agent = Voter::Clicker.new
      loop do
        sleep delay
        agent.click
        break if Time.now.hour >= 22
      end
end

set_interval(5)
